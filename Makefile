compile :
	mkdir -p bin
	javac -cp src -d bin src/*.java
	

run : compile
	java -cp bin Main


runCarte : compile
	java -cp bin MainExemplePaquetCartes


runPavage : compile
	java -cp bin MainExemplePavage


runEchec : compile
	java -cp bin MainExempleEchiquier


timeline : compile
	java -cp bin MainExempleTimeline
