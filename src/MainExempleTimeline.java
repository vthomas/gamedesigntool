import elements.Carte;
import elements.Jeton;
import elements.PaquetCartes;
import elements.Plateau;
import elements.FabriquePaquet;
import mvc.Jeu;

/**
 * classe de test 1. creer un plateau
 * 
 * @author vthomas
 *
 */
public class MainExempleTimeline {

	public static void main(String[] args) {
		// le jeu
		Jeu jeu = new Jeu();

		// ###########################
		// PLATEAU (fixe)
		// ###########################

		// pas de plateau

		// ###########################
		// GESTION PAQUET DE CARTES
		// ###########################

		// choisi la taille des cartes
		Carte.TAILLEX = 200;

		// creer le paquet recto verso avec fabrique
		FabriquePaquet fabrique = new FabriquePaquet();
		PaquetCartes paquet = fabrique.creerRectoVerso("images/timeline/");
		paquet.melanger();
		// ajoute le paquet
		jeu.addElementVariable(paquet);

		// ###########################
		// Lancement du jeu
		// ###########################

		// creation de la fenetre de la taille souhaitee
		jeu.creerAppli(1000, 800);

	}

}
