package mvc;

import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

/**
 * listener pour gerer les elements
 */
public class SourisSelection extends MouseAdapter {

	/**
	 * le jeu en cours a manipuler
	 */
	Jeu jeuEnCours;

	/**
	 * creation du listener sur le jeu
	 * 
	 * @param j le jeu a suivre
	 */
	public SourisSelection(Jeu j) {
		this.jeuEnCours = j;
	}

	@Override
	/**
	 * gestion du passage de la souris pour la selection
	 */
	public void mouseMoved(MouseEvent e) {
		super.mouseMoved(e);

		// met a jour position stockee de la souris dans jeu
		this.jeuEnCours.sourisX = e.getX();
		this.jeuEnCours.sourisY = e.getY();

		// si la souris est sur un element, il devient selection
		this.jeuEnCours.selectionneVar(e.getX(), e.getY());
	}

	@Override
	/**
	 * gestion du click de la souris => on passe l'element en premier plan
	 */
	public void mousePressed(MouseEvent e) {
		// si la souris est sur un element, il devient selection et passe en premier
		// plan
		this.jeuEnCours.choisirElement();
	}

	@Override
	/**
	 * gestion du click de la souris => on passe l'element en premier plan
	 */
	public void mouseReleased(MouseEvent e) {
		// met a jour position stockee de la souris dans jeu
		this.jeuEnCours.sourisX = e.getX();
		this.jeuEnCours.sourisY = e.getY();

		// si la souris relache un element sur un paquet
		this.jeuEnCours.relacherElement();
	}

	@Override
	/**
	 * gestion du drag de la souris => on deplace l'element selectionne
	 */
	public void mouseDragged(MouseEvent e) {

		// si la souris est sur un element, il devient selection et passe en premier
		// plan
		this.jeuEnCours.deplacerSelection(e.getX(), e.getY());
	}

}
