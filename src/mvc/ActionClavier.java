package mvc;

import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;

/**
 * Les differentes actions faisables au clavier
 * 
 * @author vthomas
 *
 */
public class ActionClavier extends KeyAdapter {

	/**
	 * le jeu en cours
	 */
	Jeu enCours;

	/**
	 * creation du controleur clavier
	 * 
	 * @param j jeu a controler
	 */
	public ActionClavier(Jeu j) {
		this.enCours = j;
	}

	@Override
	public void keyPressed(KeyEvent e) {

		// ######################
		// ACTIVATION
		// ######################

		// si la touche est espace > active l'element
		if (e.getKeyChar() == ' ') {
			this.enCours.activerSelection();
			System.out.println("- active " + this.enCours.selection);
		}

		// ######################
		// ROTATIONS
		// ######################

		// si la touche est r > lance rotation
		if (e.getKeyChar() == 'r') {
			this.enCours.rotationSelection(true);
			System.out.println("- rotation " + this.enCours.selection);

		}
		// si la touche est a > lance rotation directe
		if (e.getKeyChar() == 'a') {
			this.enCours.rotationSelection(false);
			System.out.println("- rotation " + this.enCours.selection);

		}

		// si la touche est e > lance rotation indirecte
		if (e.getKeyChar() == 'e') {
			this.enCours.rotationSelection(true);
			System.out.println("- rotation " + this.enCours.selection);

		}

		// ######################
		// MELANGE
		// ######################

		// si la touche est s > lance melange
		if (e.getKeyChar() == 's') {
			this.enCours.melangerElementSelect();
			System.out.println("- melange " + this.enCours.selection);
		}

		// ######################
		// GESTION TAILLE
		// ######################

		// si la touche est + > augmente la taille
		if (e.getKeyChar() == '+') {
			this.enCours.AugmenterElementSelect();
			System.out.println("- augmente " + this.enCours.selection);
		}

		// si la touche est - > diminue la taille
		if (e.getKeyChar() == '-') {
			this.enCours.DiminuerElementSelect();
			System.out.println("- diminue " + this.enCours.selection);
		}

		// si la touche est 0 > reinitialise la taille
		if (e.getKeyChar() == '0') {
			this.enCours.ReinitialiserTailleElementSelect();
			System.out.println("- reinit taille " + this.enCours.selection);
		}

		// si la touche est b > passe l'element en bas des elements
		if (e.getKeyChar() == 'b') {
			this.enCours.PasserBasElementSelect();
			System.out.println("- element en bas de la pile : " + this.enCours.selection);
		}

		// si la touche est l > locke element : devient no deplacable
		if (e.getKeyChar() == 'l') {
			this.enCours.LockerElementSelect();
			System.out.println("- element locke : " + this.enCours.selection);
		}

		// si la touche est u > delocke element : devient no deplacable
		if (e.getKeyChar() == 'u') {
			this.enCours.UnlockerElementSelect();
			System.out.println("- element delocke : " + this.enCours.selection);
		}

		// si la touche est f > flip l'element
		if (e.getKeyChar() == 'f') {
			this.enCours.FlipSelected();
			System.out.println("- element flip : " + this.enCours.selection);
		}

		// si la touche est suppr => supprime element
		if (e.getKeyCode() == KeyEvent.VK_DELETE) {
			this.enCours.deleteSelect();
			System.out.println("- supprime " + this.enCours.selection);
		}

	}

}
