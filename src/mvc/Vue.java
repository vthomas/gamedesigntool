package mvc;

import java.awt.Graphics;
import java.util.Observable;
import java.util.Observer;

import javax.swing.JPanel;

import elements.Element;

/**
 * Vue du jeu (utilise observer)
 * 
 * @author vthomas
 *
 */
@SuppressWarnings("deprecation")
public class Vue extends JPanel implements Observer {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	/**
	 * le jeu en cours a afficher
	 */
	Jeu jeuEnCours;

	/**
	 * le jeu correspondant
	 * 
	 * @param jeu jeu a afficher
	 */
	public Vue(Jeu jeu) {
		this.jeuEnCours = jeu;
		// s'ajoute en tant qu'observer
		this.jeuEnCours.addObserver(this);
	}

	/**
	 * affichage de la vue
	 * 
	 * @param g dessin de la vue
	 */
	public void paint(Graphics g) {
		// rafraichit image
		super.paint(g);

		// affiche les elements fixes
		for (Element m : this.jeuEnCours.elementsFixes)
			m.drawElement(g);

		// affiche les elements variables
		for (Element m : this.jeuEnCours.elementsVar)
			m.drawElement(g);

		// affiche l'element selectionne s'il existe
		// test si l'element existe
		if (this.jeuEnCours.selection != null) {
			// appelle la methode de surlignage
			this.jeuEnCours.selection.drawElementSelect(g);
		}
	}

	@Override
	public void update(Observable o, Object arg) {
		// demande reaffichage
		this.repaint();
	}

}
