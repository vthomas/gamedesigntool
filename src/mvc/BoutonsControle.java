package mvc;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.*;

import javax.swing.JButton;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JPanel;

/**
 * panel de boutons pour la sauvegarde / chargement
 * 
 * @author vthomas
 *
 */
public class BoutonsControle extends JPanel implements ActionListener {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	/**
	 * le jeu en cours
	 */
	Jeu jeuEnCours;

	/**
	 * creation d'un controleur pour manipuler le jeu
	 * 
	 * @param jeu jeu a manipuler
	 */
	public BoutonsControle(Jeu jeu) {
		this.jeuEnCours = jeu;

		// creation des boutons
		JButton sauver = new JButton("sauver");
		JButton charger = new JButton("charger");

		// TODO
		// charger.setEnabled(false);
		// sauver.setEnabled(false);

		// ajout des boutons (lineaires)
		this.add(sauver);
		this.add(charger);

		// ajout des listener (soi meme)
		sauver.addActionListener(this);
		charger.addActionListener(this);

	}

	// methode appelee quand on clique sur un des boutons
	public void actionPerformed(ActionEvent arg0) {
		// fonctionne differemment en fonction du parent
		// TODO a modifier
		JButton source = (JButton) (arg0.getSource());
		String actionName = source.getText();
		System.out.println("- action " + actionName);

		// en fonction de l'action
		switch (actionName) {
		case "charger":
			// charge le jeu
			this.charger();
			break;
		case "sauver":
			// sauve le jeu
			this.sauver();
			break;
		default:
			throw new Error("action bouton inconnue");
		}

		// redonne le focus a la JFrame
		((JFrame) (this.getParent().getParent().getParent().getParent())).requestFocus();

	}

	/**
	 * sauve le jeu en cours
	 */
	private void sauver() {
		// gestion d'une fenetre de choix de fichier
		JFileChooser fc = new JFileChooser();
		fc.setCurrentDirectory(new File("./saveDir/"));

		// lancement de la fenetre
		int retour = fc.showOpenDialog(null);

		// en fonction de la valeur de retour
		if (retour == JFileChooser.APPROVE_OPTION) {
			// afficher le fichier
			File file = fc.getSelectedFile();
			System.out.println("- Sauve Nom du fichier : " + file.getName());

			// exporte jeu dans le fichier
			try {
				// genere le flux pour serialisation
				ObjectOutputStream fluxOut = new ObjectOutputStream(new FileOutputStream(file));
				// sauve objet
				fluxOut.writeObject(this.jeuEnCours);
				// ferme le flux
				fluxOut.close();

			} catch (FileNotFoundException e) {
				System.err.println("**ERREUR**: fichier non trouve");
			} catch (IOException e) {
				e.printStackTrace();
				System.err.println("**ERREUR**: problem IO");
			}
			// annulation
			System.out.println("- fin sauvgerdae");

		} else {
			// annulation
			System.out.println("- ouverture annulee");
		}

	}

	/**
	 * charge le jeu en cours
	 */
	private void charger() {
		// gestion d'une fenetre de choix de fichier
		JFileChooser fc = new JFileChooser();
		fc.setCurrentDirectory(new File("./saveDir/"));

		// lancement de la fenetre
		int retour = fc.showOpenDialog(null);

		// en fonction de la valeur de retour
		if (retour == JFileChooser.APPROVE_OPTION) {
			// afficher le fichier
			File file = fc.getSelectedFile();
			System.out.println("- Charge Nom du fichier : " + file.getName());

			// importe le jeu dans le fichier
			try {
				// genere le flux pour serialisation
				ObjectInputStream fluxIn = new ObjectInputStream(new FileInputStream(file));

				// charge objet
				Jeu nouveau = (Jeu) (fluxIn.readObject());
				// reinitialiser jeu encours
				this.jeuEnCours.reinitialise(nouveau);

				// ferme le flux
				fluxIn.close();

			} catch (FileNotFoundException e) {
				System.err.println("**ERREUR**: fichier non trouve");
			} catch (IOException e) {
				e.printStackTrace();
				System.err.println("**ERREUR**: problem IO");
			} catch (ClassNotFoundException e) {
				e.printStackTrace();
				System.err.println("**ERREUR**: classe non reconnue");
			}
			// annulation
			System.out.println("- fin chargement");

		} else {
			// annulation
			System.out.println("- ouverture annulee");
		}

	}

}
