package mvc;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.io.File;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Observable;

import javax.swing.JFrame;

import elements.*;

@SuppressWarnings("deprecation")
/**
 * les elements a afficher
 * 
 * @author vthomas
 *
 */
public class Jeu extends Observable implements Serializable {

	/**
	 * serial ID
	 */
	private static final long serialVersionUID = -2832113476770189851L;

	/**
	 * derniere position souris
	 */
	int sourisX, sourisY;

	/**
	 * les elements de jeu fixes
	 */
	List<Element> elementsFixes;

	/**
	 * les elements de jeu deplacables
	 */
	List<Element> elementsVar;

	/**
	 * Element selectionne
	 */
	Element selection = null;

	// #######################
	// GESTION DES ELEMENTS
	// ######################

	/**
	 * construit un modele vide
	 */
	public Jeu() {
		this.elementsFixes = new ArrayList<>();
		this.elementsVar = new ArrayList<>();
	}

	/**
	 * ajoute un element fixe
	 * 
	 * @param e element ajoute
	 */
	public void addElementFixe(Element e) {
		this.elementsFixes.add(e);
	}

	/**
	 * ajoute un element variable
	 * 
	 * @param e element ajoute
	 */
	public void addElementVariable(Element e) {
		this.elementsVar.add(e);
	}

	// ############################
	// GESTION DES JETONS
	// ############################

	/**
	 * ajoute un jeton au jeu
	 * 
	 * @param filename image du jeton
	 */
	public void addJeton(String filename) {
		this.elementsVar.add(new Jeton(filename));
	}

	/**
	 * ajoute tous les jetons d'un repertoire
	 * 
	 * @param repertoireNom nom du repertoire
	 */
	public void addTousLesJetons(String repertoireNom) {
		// ouvre le repertoire
		File rep = new File(repertoireNom);

		// verifie repertoire
		if (!rep.isDirectory())
			throw new Error(repertoireNom + ": ce n'est pas un repertoire valide");

		// prend toutes les images
		File[] files = rep.listFiles();
		for (File fichier : files) {
			// recupere extension
			String ext = fichier.getName().substring(fichier.getName().lastIndexOf("."));

			// si extension est correcte
			if (ext.equals(".png") || ext.equals(".jpg") || ext.equals(".gif")) {
				// ajoute le jeton correspondant
				this.addJeton(fichier.getPath());
			}

		}

	}

	// ############################
	// GESTION DES CARTES
	// ############################

	/**
	 * ajoute une carte au jeu
	 * 
	 * @param filename nom du fichier de la carte
	 */
	public void addCarte(String filename) {
		this.elementsVar.add(new Carte(filename));
	}

	/**
	 * ajoute toutes les cartes d'un repertoire
	 * 
	 * @param repertoireNom noim du repertoire
	 */
	public void addToutesLesCarte(String repertoireNom) {
		// charge toutes les cartes
		List<Carte> cartes = Carte.chargerDesCartes(repertoireNom);

		// ajoute les cartes une a une
		for (Carte c : cartes)
			this.elementsVar.add(c);
	}

	// ############################
	// GESTION DE LA SELECTION
	// ############################

	/**
	 * permet de selectionner un element variable dans l'image
	 * 
	 * @param x     position sur l'image
	 * @param y     position sur l'image
	 * @param liste liste testee
	 */
	public void selectionneListe(int x, int y, List<Element> elements) {
		// cherche parmi les elements variables en partant de la fin
		for (int i = elements.size() - 1; i >= 0; i--) {
			Element elem = elements.get(i);
			// si le point est dans l'element
			if (elem.isInside(x, y)) {
				// l'element devient selectionne et on s'arrete
				this.selection = elem;
				this.setChanged();
				this.notifyObservers();
				return;
			}
		}

		// aucun element selectionne
		this.selection = null;
		this.setChanged();
		this.notifyObservers();
	}

	/**
	 * permet de selectionner un element variable dans l'image
	 * 
	 * @param x     position sur l'image
	 * @param y     position sur l'image
	 * @param liste liste testee
	 */
	public void selectionneVar(int x, int y) {
		this.selectionneListe(x, y, this.elementsVar);
	}

	/**
	 * passe l'element en premier plan
	 */
	public void choisirElement() {
		if (this.selection != null) {
			this.elementsVar.remove(this.selection);
			this.elementsVar.add(this.selection);
		}
	}

	/**
	 * deplace la selection et met a jour
	 * 
	 * @param x nouveau x
	 * @param y nouveau y
	 */
	public void deplacerSelection(int x, int y) {
		if (selection != null) {
			this.selection.deplacer(x, y);
			this.setChanged();
			this.notifyObservers();
		}
	}

	// #############################
	// ACTION ELEMENT SELECTIONNE
	// ############################

	/**
	 * activer l'elemnt selectionne
	 */
	public void activerSelection() {
		if (this.selection != null) {
			this.selection.activer(this);
			this.setChanged();
			this.notifyObservers();
		}
	}

	/**
	 * faire rotation de l'elemen
	 * 
	 * @param sens sens de rotation
	 */
	public void rotationSelection(boolean sens) {
		if (this.selection != null) {
			this.selection.rotation(sens);
			this.setChanged();
			this.notifyObservers();
		}
	}

	/**
	 * melanger l'element
	 */
	public void melangerElementSelect() {
		if (this.selection != null) {
			this.selection.melanger();
			this.setChanged();
			this.notifyObservers();
		}

	}

	/**
	 * augmenter la taille
	 */
	public void AugmenterElementSelect() {
		if (this.selection != null) {
			this.selection.augmenterTaille();
			this.setChanged();
			this.notifyObservers();
		}
	}

	/**
	 * diminue taille element selectionne
	 */
	public void DiminuerElementSelect() {
		if (this.selection != null) {
			this.selection.diminuerTaille();
			this.setChanged();
			this.notifyObservers();
		}
	}

	/**
	 * diminue taille element selectionne
	 */
	public void ReinitialiserTailleElementSelect() {
		if (this.selection != null) {
			this.selection.reinitialiserTaille();
			this.setChanged();
			this.notifyObservers();
		}
	}

	/**
	 * passe un element au dernier plan
	 */
	public void PasserBasElementSelect() {
		if (this.selection != null) {
			// retirer l'element de la liste
			this.elementsVar.remove(this.selection);
			// remettre en tete de la liste
			this.elementsVar.add(0, this.selection);

			// modifie element selectionne
			this.selectionneVar(this.sourisX, this.sourisY);

			this.setChanged();
			this.notifyObservers();
		}
	}

	/**
	 * locker element selectionne
	 */
	public void LockerElementSelect() {
		if (this.selection != null) {
			// retirer l'element de la liste
			this.elementsVar.remove(this.selection);
			// remettre dans la liste fixe
			this.elementsFixes.add(this.selection);

			this.setChanged();
			this.notifyObservers();
		}

	}

	/**
	 * unlocker element
	 */
	public void UnlockerElementSelect() {
		// selectionne element correspondant parmi les fixes
		this.selectionneListe(this.sourisX, this.sourisY, this.elementsFixes);

		// si il y en a un, le changer de categorie
		if (this.selection != null) {
			// retirer l'element de la liste fixe
			this.elementsFixes.remove(this.selection);
			// remettre dans la liste variable
			this.elementsVar.add(0, this.selection);

			this.setChanged();
			this.notifyObservers();
		}

	}

	/**
	 * retourner l'element en cours
	 */
	public void FlipSelected() {
		// si selection, faire flip
		if (this.selection != null) {
			// flip
			this.selection.flip();

			this.setChanged();
			this.notifyObservers();
		}
	}

	/**
	 * supprime element selectionne
	 */
	public void deleteSelect() {
		// si selection, faire flip
		if (this.selection != null) {
			// supprime
			this.elementsVar.remove(this.selection);
			this.selection = null;

			this.setChanged();
			this.notifyObservers();
		}

	}

	// ############################
	// CREATION DE L APPLI
	// ############################

	/**
	 * reinitialise j a partir des donnees de jeu
	 * 
	 * @param jeu jeu contenant les donnees pour reinitialisation
	 */
	public void reinitialise(Jeu jeu) {
		// recopie dans jeuEncours
		this.elementsFixes = jeu.elementsFixes;
		this.elementsVar = jeu.elementsVar;
		this.selection = null;

		// recharge les elements
		for (Element el : this.elementsFixes) {
			el.reload();
		}
		// recharge les elements
		for (Element el : this.elementsVar) {
			el.reload();
		}
	}

	// recharge toutes les images

	// ############################
	// CREATION DE L APPLI
	// ############################

	/**
	 * creer une fenetre avec les elements
	 * 
	 * @param tailleFenetreX taille de la fenetre en X
	 * @param tailleFenetreY taille de la fenetre en Y
	 * 
	 */
	public JFrame creerAppli(int tailleFenetreX, int tailleFenetreY) {
		// creer la JFrame
		JFrame f = new JFrame("Jeu");
		f.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

		// gerer les borderlayout de la frame
		f.getContentPane().setLayout(new BorderLayout());

		// #################
		// Boutons
		// #################

		// creer le panel de bouton
		BoutonsControle boutons = new BoutonsControle(this);
		f.getContentPane().add(boutons, BorderLayout.SOUTH);

		// #################
		// VUE DU JEU
		// #################

		// creer la vue autour du jeu courant
		Vue v = new Vue(this);
		v.setPreferredSize(new Dimension(tailleFenetreX, tailleFenetreY));

		// mettre tout ensemble
		f.getContentPane().add(v, BorderLayout.CENTER);

		// #################
		// LISTENER SUR LA VUE
		// #################

		// creation du listener souris
		SourisSelection elemSelect = new SourisSelection(this);
		v.addMouseListener(elemSelect);
		v.addMouseMotionListener(elemSelect);

		// creation controller clavier
		ActionClavier clavier = new ActionClavier(this);
		f.addKeyListener(clavier);

		// lancer l'affichage
		f.pack();
		f.setVisible(true);
		f.requestFocus();

		// retourner la Jframe
		return f;
	}

	/**
	 * relacher un element (carte sur un paquet)
	 */
	public void relacherElement() {
		// si un element a ete selectionne
		if (this.selection != null) {

			// TODO a modifier a l'occasion (eviter des instance of dans le code)
			// TODO creer classe "un packet de" et jouer sur la genericite

			// si c'est une carte
			if (this.selection instanceof Carte) {
				// cherche le premier paquet au meme endroit
				for (int i = this.elementsVar.size() - 1; i >= 0; i--) {
					// recupere l'element
					Element elem = this.elementsVar.get(i);
					// si c'est un paquet et que la carte est lachee au bon endroit, ajoute la carte
					if ((elem instanceof PaquetCartes) && (elem.isInside(this.sourisX, this.sourisY))) {

						// ajoute la carte en fin de paquet
						PaquetCartes paquet = (PaquetCartes) elem;
						Carte carte = (Carte) this.selection;
						paquet.ajouterCarte(carte);

						// retire la carte du jeu visible
						this.elementsVar.remove(this.selection);

						// met a jour la selection
						this.selection = paquet;

						// reaffiche
						this.setChanged();
						this.notifyObservers();

						// fin boucle
						break;
					}
				}
			}
		}

	}

}
