package tool;

import java.awt.Graphics2D;
import java.awt.image.BufferedImage;

/**
 * classe outil pour faire des rotations d'image
 * 
 * @author vthomas
 *
 */
public class ImageRotate {

	/**
	 * construit une nouvelle image par rotation
	 * 
	 * @param src  image source sur laquelle appliquer une rotation
	 * @param sens sens de rotation : true = PI/2 , false = -Pi/2
	 * @return image apres rotation
	 */
	public static BufferedImage rotation(BufferedImage src, boolean sens) {
		int width = src.getWidth();
		int height = src.getHeight();

		// image de destination
		BufferedImage dest = new BufferedImage(height, width, src.getType());

		// creation d'un dessin pour la rotation
		Graphics2D graphics2D = dest.createGraphics();
		if (sens) {
			// rotation directe
			graphics2D.translate((height - width) / 2, (height - width) / 2);
			graphics2D.rotate(Math.PI / 2, height / 2, width / 2);

		} else {
			// rotation sens directe
			graphics2D.translate((width - height) / 2, (width - height) / 2);
			graphics2D.rotate(3 * Math.PI / 2, height / 2, width / 2);
		}
		graphics2D.drawRenderedImage(src, null);

		return dest;
	}

}
