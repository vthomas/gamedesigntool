package tool;

import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.HashMap;

import javax.imageio.ImageIO;

/**
 * class to store and have easily access to images in a static map
 */
public class ImageLoader {

    /**
     * map containing the stored images
     */
    static HashMap<String, BufferedImage> images = new HashMap<>();

    /**
     * load and store an image
     *
     * @param name name of the stored image
     * @param path path to load image
     */
    public static void loadImage(String name, String path){
        // load image
        BufferedImage image = null;
        System.out.println("- charge " + path);
        try {
            image = ImageIO.read(new File(path));
        } catch (IOException e) {
            System.out.println(" - #### Chemin inconnu : " + path);
            //e.printStackTrace();
        }

        // store image
        images.put(name,image);
    }


    public static BufferedImage get(String name) {
        return images.get(name);
    }
}
