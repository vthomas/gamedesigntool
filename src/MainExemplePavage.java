import java.util.Random;

import elements.Carte;
import elements.PaquetCartes;
import mvc.Jeu;

public class MainExemplePavage {

	// construit un plateau avec des cartes
	public static void main(String[] args) {
		// le jeu
		Jeu jeu = new Jeu();

		// taille du plateau
		int tailleX = 5;
		int tailleY = 5;

		// choix de la taille des cartes
		int TailleCarte = 100;
		Carte.TAILLEX = TailleCarte;

		// generateur de nombre aleatoire
		Random rand = new Random();

		// construit les tuiles une par une dans une boucle
		for (int i = 0; i < tailleX; i++)
			for (int j = 0; j < tailleX; j++) {
				// tire au hasard un nombre pour la tuile (1,2,3)
				int num = rand.nextInt(3) + 1;

				// construit nom de l'image
				String nomImage = "images/pavage/pavage" + num + ".png";

				// creer la carte
				Carte c = new Carte(nomImage);

				// la positionne (taille x et y identique car carte carree)
				c.deplacer(i * TailleCarte + 100, j * TailleCarte + 100);

				// l'ajoute au jeu
				jeu.addElementVariable(c);
			}
		
		//ajoute un paquet vide (pour ranger les tuiles)
		PaquetCartes vide = new PaquetCartes();
		vide.deplacer(500, 700);
		jeu.addElementVariable(vide);
		

		// ###########################
		// Lancement du jeu
		// ###########################

		// creation de la fenetre de la taille souhaitee
		jeu.creerAppli(1000, 800);

	}

}
