package elements;

import java.util.*;

/**
 * classe qui permet de fabriquer un paquet de cartes
 */
public class FabriquePaquet{

  /** 
   * construit un paquet de cartes recto-verso
   * cherche les cartes avec le meme nom dans sous repertoire recto et verso
   *
   * @param repertoire : nom du repertoire
   */
  public PaquetCartes creerRectoVerso(String nom) {
	// creer la liste d'images recto verso
	List<CarteRectoVerso> cartes = CarteRectoVerso.chargerDesCartesRV(nom);

	// construit le paquet	
	PaquetCartes paquet = new PaquetCartes();

	// ajoute cartes une a une
	for (Carte c : cartes){
		paquet.ajouterCarte(c);
	}

	// le retourne
	return paquet;	
  }


}
