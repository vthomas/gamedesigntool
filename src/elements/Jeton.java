package elements;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.image.BufferedImage;

import mvc.Jeu;
import tool.ImageLoader;
import tool.ImageRotate;

/**
 * permet de modeliser un jeton
 * 
 * @author vthomas
 *
 */
public class Jeton extends Element {

	/**
	 * serial ID
	 */
	private static final long serialVersionUID = -83709096657676130L;

	// ################################
	// position initiales des jetons
	// ################################

	/**
	 * taille par defaut
	 */
	public static int TAILLE = 50;
	public static int DECALAGE = 10;

	/**
	 * positionnement initial des jetons
	 */
	public static int POS_INIT_X = 0;
	public static int POS_INIT_Y = 0;

	/**
	 * placer position initiale de la suite de jetons
	 */
	public static void setPositionInit(int x, int y) {
		POS_INIT_X = x;
		POS_INIT_Y = y;
	}

	// ################################
	// jeton en cours
	// ################################

	/**
	 * la taille du jeton (suppose carre)
	 */
	int taille;

	/**
	 * la taille initiale du jeton
	 */
	int tailleInit;

	/**
	 * face du jeton
	 */
	boolean recto = true;

	/**
	 * l'image correspondante au jeton (non serialisee)
	 */
	transient BufferedImage image;

	/**
	 * nom du jeton
	 */
	private String fileName;

	/**
	 * creer un jeton par defaut
	 * 
	 * @param image nom de l'image
	 */
	public Jeton(String fileName) {

		// localise en x,y
		super(POS_INIT_X, POS_INIT_Y);

		// ajoute le nom
		this.fileName = fileName;

		// taille par defaut du jeton
		this.taille = TAILLE;
		this.tailleInit = this.taille;

		// decale la position du futur jeton
		POS_INIT_X += DECALAGE;

		// chargement de l'image a travers la classe ImageLoader
		ImageLoader.loadImage(fileName, fileName);
		this.image = ImageLoader.get(fileName);
	}

	@Override
	/**
	 * dessine le jeton (interface Dessinable)
	 */
	public void drawElement(Graphics g) {
		if (recto) {
			g.drawImage(this.image, posx, posy, taille, taille, null);
		} else {
			g.setColor(Color.red);
			g.fillRect(posx, posy, taille, taille);
			g.setColor(Color.black);
			g.drawRect(posx, posy, taille, taille);
		}
	}

	@Override
	public void activer(Jeu jeu) {
		this.flip();
	}

	@Override
	/**
	 * dessine le jeton selectionne (interface Dessinable)
	 */
	public void drawElementSelect(Graphics g) {
		this.drawElement(g);
		g.drawRect(posx, posy, taille, taille);
	}

	@Override
	// verifie si dans carre
	public boolean isInside(int x, int y) {
		return (x > this.posx) && (y > this.posy) && (x < this.posx + this.taille) && (y < this.posy + this.taille);
	}

	@Override
	// deplacer au centre du jeton
	public void deplacer(int nx, int ny) {
		this.posx = nx - this.taille / 2;
		this.posy = ny - this.taille / 2;
	}

	// effectue une rotation du jeton
	public void rotation(boolean sens) {
		super.rotation(sens);
		// fait une rotation de l'image
		this.image = ImageRotate.rotation(this.image, sens);
	}

	public String toString() {
		return "Jeton " + this.fileName;
	}

	// ######################
	// TAILLE JETON
	// ######################

	@Override
	public void augmenterTaille() {
		this.taille *= 1.1;
		// au cas ou trop petit pour regrandir
		this.taille++;
	}

	@Override
	public void diminuerTaille() {
		this.taille /= 1.1;
	}

	@Override
	public void reinitialiserTaille() {
		this.taille = this.tailleInit;
	}

	@Override
	public void flip() {
		// change jeton de face
		this.recto = !this.recto;
	}

	@Override
	public void reload() {
		// recharge image du jeton
		// chargement de l'image a travers la classe ImageLoader
		ImageLoader.loadImage(this.fileName, this.fileName);
		this.image = ImageLoader.get(this.fileName);
	}

}
