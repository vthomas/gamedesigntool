package elements;

import java.awt.Graphics;

/**
 * interface correspondant aux objets dessinables
 * 
 * @author vthomas
 *
 */
public interface Dessinable {

	/**
	 * methode a appeler pour dessiner l'element
	 * 
	 * @param g graphics dans lequel dessiner
	 */
	public void drawElement(Graphics g);
	
	/**
	 * methode a appeler pour dessiner l'element lorsque selectionne 
	 * 
	 * @param g graphics dans lequel dessiner
	 */
	public void drawElementSelect(Graphics g);

}
