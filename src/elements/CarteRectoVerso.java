package elements;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.image.BufferedImage;
import java.io.File;
import java.util.ArrayList;
import java.util.List;

import mvc.Jeu;
import tool.ImageLoader;
import tool.ImageRotate;

/**
 * permet de modeliser un jeton
 * 
 * @author vthomas
 *
 */
public class CarteRectoVerso extends Carte {

	
	/**
	 * l'image correspondante au verso
	 */
	transient BufferedImage imageVerso;

	/**
	 * nom de l'image verso
	 */
	String verso;

	/**
	 * creer un jeton par defaut
	 * 
	 * @param image nom de l'image
	 */
	public CarteRectoVerso(String fileName, String verso) {
		// genere recto carte
		super(fileName);
		

		// chargement de l'image verso a travers la classe ImageLoader
		ImageLoader.loadImage(verso, verso);
		this.imageVerso = ImageLoader.get(verso);
		this.verso = verso;

	}

	@Override
	/**
	 * dessine la carte (interface Dessinable)
	 */
	public void drawElement(Graphics g) {
		if (recto) {
			g.drawImage(this.image, posx, posy, tailleX, tailleY, null);
		} else {
			g.drawImage(this.imageVerso, posx, posy, tailleX, tailleY, null);
		}
	}

	@Override
	public void reload() {
		// recharge les elements transient (ici image)
		ImageLoader.loadImage(this.fileName, this.fileName);
		this.image = ImageLoader.get(this.fileName);
		ImageLoader.loadImage(this.verso, this.verso);
		this.imageVerso = ImageLoader.get(this.verso);
	}

	/**
	 * permet de charger des cartes recto verso a partir d'un repertoire
	 * 
	 * @param repertoireNom nom du repertoire (avec sous repertorie recto et verso)
	 * @return la liste des cartes chargees
	 */
	public static List<CarteRectoVerso> chargerDesCartesRV(String repertoireNom) {
		// creation de la liste resultat
		List<CarteRectoVerso> res = new ArrayList<>();

		// repertoires
		String repRecto = repertoireNom+"/recto";
		String repVerso = repertoireNom+"/verso";

		// ouvre le repertoire recto
		File rep = new File(repRecto);
		File verso = new File(repVerso);

		// verifie repertoire
		if (!rep.isDirectory())
			throw new Error(repRecto + ": ce n'est pas un repertoire valide, il faut des sous repertoire recto et verso");
		// verifie repertoire
		if (!verso.isDirectory())
			throw new Error(repVerso + ": ce n'est pas un repertoire valide, il faut des sous repertoire recto et verso");

		// prend toutes les images de recto
		File[] files = rep.listFiles();
		for (File fichier : files) {
			System.out.println(fichier.getName());
			// recupere extension
			String ext = fichier.getName().substring(fichier.getName().lastIndexOf("."));

			// si extension est correcte
			if (ext.equals(".png") || ext.equals(".jpg") || ext.equals(".gif")) {
				// cherche image dans verso
				File fichierverso = new File(repVerso+"/"+fichier.getName());
				
				// creer la carteRectoVerso
				CarteRectoVerso c = new CarteRectoVerso(fichier.getPath(),fichierverso.getPath());

				// ajoute la carte correspondant
				res.add(c);
			}
		}

		// fin du parcours => resultats
		return res;
	}
}
