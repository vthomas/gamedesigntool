package elements;

import java.awt.Color;
import java.awt.Graphics;
import java.util.Random;

import mvc.Jeu;

/**
 * gestion de De
 * 
 * @author vthomas
 *
 */
public class De extends Element {

	/**
	 * serila ID
	 */
	private static final long serialVersionUID = 1L;

	public static int POSX = 100;
	public static int POSY = 200;
	public static int DECAL = 50;
	public static int TAILLE = 50;

	/**
	 * parite du de (pour montrer le lancer)
	 */
	boolean pair = true;

	/**
	 * nombre de faces du de
	 */
	int nbFaces;

	/**
	 * valeur courante
	 */
	int valeurCourante;

	/**
	 * taille du de
	 */
	int taille = TAILLE;

	/**
	 * generateur aleatoire associe
	 */
	Random rand = new Random();

	/**
	 * constructeur de De par defaut (6 faces)
	 */
	public De() {
		super(POSX, POSY);
		POSX += DECAL;
		this.nbFaces = 6;
		this.lancerDe();
	}

	/**
	 * constructeur de De a x faces (X faces)
	 */
	public De(int faces) {
		super(POSX, POSY);
		POSX += DECAL;
		this.nbFaces = faces;
		this.lancerDe();
	}

	/**
	 * activer un de consiste a lancer le de
	 */
	public void activer(Jeu j) {
		this.lancerDe();
	}

	/**
	 * lancer le de
	 */
	public void lancerDe() {
		this.valeurCourante = rand.nextInt(nbFaces) + 1;
		this.pair = !this.pair;
	}

	@Override
	public void drawElement(Graphics g) {
		// dessiner carre
		if (this.pair)
			g.setColor(Color.blue);
		else
			g.setColor(Color.gray);
		g.fillRect(posx, posy, taille, taille);

		// dessiner la valeur
		g.setColor(Color.yellow);
		g.drawString("De" + this.nbFaces, posx + taille / 4, posy + (1 * taille) / 4);
		g.drawString("" + this.valeurCourante, posx + taille / 2 - 5, posy + (3 * taille) / 4);
	}

	@Override
	public void drawElementSelect(Graphics g) {
		this.drawElement(g);
		g.setColor(Color.black);
		g.drawRect(posx, posy, taille, taille);
	}

	@Override
	public boolean isInside(int x, int y) {
		return (x > this.posx) && (y > this.posy) && (x < this.posx + this.taille) && (y < this.posy + this.taille);
	}

	@Override
	// deplacer au centre du de
	public void deplacer(int nx, int ny) {
		this.posx = nx - this.taille / 2;
		this.posy = ny - this.taille / 2;
	}

	@Override
	public void reload() {
		// pas d'attribut transient a gerer
	}

	@Override
	public void flip() {
		// retourne le de : valeur opposee
		this.valeurCourante = this.nbFaces - this.valeurCourante + 1;
	}

}
