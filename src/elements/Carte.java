package elements;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.image.BufferedImage;
import java.io.File;
import java.util.ArrayList;
import java.util.List;

import mvc.Jeu;
import tool.ImageLoader;
import tool.ImageRotate;

/**
 * permet de modeliser un jeton
 * 
 * @author vthomas
 *
 */
public class Carte extends Element {

	/**
	 * serial ID
	 */
	private static final long serialVersionUID = 7264867789387643112L;

	/**
	 * taille par defaut
	 */
	public static int TAILLEX = 50;
	public static int DECALAGE = 10;

	/**
	 * positionnement initial des cartes
	 */
	static int posInitX = 0;
	static int posInitY = 0;

	/**
	 * la taille de la carte (suppose carre)
	 */
	int tailleX, tailleY;

	/**
	 * taille initiale
	 */
	int tailleInitX, tailleInitY;

	/**
	 * boolean correpondant a la face de la carte
	 */
	boolean recto = true;

	/**
	 * l'image correspondante au jeton
	 */
	transient BufferedImage image;

	/**
	 * nom de l'image
	 */
	String fileName;

	/**
	 * creer un jeton par defaut
	 * 
	 * @param image nom de l'image
	 */
	public Carte(String fileName) {
		// localise en x,y
		super(posInitX, posInitY);

		// taille par defaut de la carte
		this.tailleX = TAILLEX;
		// decale la position du futur jeton
		posInitX += DECALAGE;

		// chargement de l'image a travers la classe ImageLoader
		ImageLoader.loadImage(fileName, fileName);
		this.image = ImageLoader.get(fileName);
		this.fileName = fileName;

		// taille par defaut de la carte (respecte ratio)
		this.tailleY = (int) (TAILLEX * 1.0 * this.image.getHeight(null) / this.image.getWidth(null));

		// stocke les tailles initiales
		this.tailleInitX = this.tailleX;
		this.tailleInitY = this.tailleY;
	}

	@Override
	/**
	 * dessine la carte (interface Dessinable)
	 */
	public void drawElement(Graphics g) {
		if (recto) {
			g.drawImage(this.image, posx, posy, tailleX, tailleY, null);
		} else {
			g.setColor(Color.red);
			g.fillRect(posx, posy, tailleX, tailleY);
			g.setColor(Color.black);
			g.drawRect(posx, posy, tailleX, tailleY);
		}
	}

	@Override
	// activer une carte consiste a la retourner
	public void activer(Jeu jeu) {
		this.flip();

	}

	@Override
	// augmenter taille
	public void augmenterTaille() {
		this.tailleX *= 1.1;
		this.tailleY *= 1.1;

		// au cas ou en passant en entier, plus d'augmentation
		this.tailleX++;
		this.tailleY++;

	}

	@Override
	// diminuer taille
	public void diminuerTaille() {
		this.tailleX /= 1.1;
		this.tailleY /= 1.1;

	}

	@Override
	// reinit taille
	public void reinitialiserTaille() {
		this.tailleX = this.tailleInitX;
		this.tailleY = this.tailleInitY;

	}

	@Override
	// effectue une rotation de la carte
	public void rotation(boolean sens) {
		super.rotation(sens);

		// fait une rotation de l'image
		this.image = ImageRotate.rotation(this.image, sens);

		// decalage a faire
		// this.deplacer(-this.tailleX, -this.tailleY);

		// change les dimensions
		int temp = this.tailleX;
		this.tailleX = this.tailleY;
		this.tailleY = temp;

	}

	@Override
	/**
	 * dessine le jeton selectionne (interface Dessinable)
	 */
	public void drawElementSelect(Graphics g) {
		this.drawElement(g);
		g.setColor(Color.black);
		g.drawRect(posx, posy, tailleX, tailleY);
	}

	@Override
	// verifie si dans carre
	public boolean isInside(int x, int y) {
		return (x > this.posx) && (y > this.posy) && (x < this.posx + this.tailleX) && (y < this.posy + this.tailleY);
	}

	@Override
	// deplacer au centre du jeton
	public void deplacer(int nx, int ny) {
		this.posx = nx - this.tailleX / 2;
		this.posy = ny - this.tailleY / 2;
	}

	/**
	 * permet de charger des cartes a partir d'un repertoire
	 * 
	 * @param repertoireNom nom du repertoire
	 * @return la liste des cartes chargees
	 */
	public static List<Carte> chargerDesCartes(String repertoireNom) {
		// creation de la liste resultat
		List<Carte> res = new ArrayList<>();

		// ouvre le repertoire
		File rep = new File(repertoireNom);

		// verifie repertoire
		if (!rep.isDirectory())
			throw new Error(repertoireNom + ": ce n'est pas un repertoire valide");

		// prend toutes les images
		File[] files = rep.listFiles();
		for (File fichier : files) {
			System.out.println(fichier.getName());
			// recupere extension
			String ext = fichier.getName().substring(fichier.getName().lastIndexOf("."));

			// si extension est correcte
			if (ext.equals(".png") || ext.equals(".jpg") || ext.equals(".gif")) {
				// creer la carte
				Carte c = new Carte(fichier.getPath());

				// ajoute la carte correspondant
				res.add(c);
			}
		}

		// fin du parcours => resultats
		return res;
	}

	@Override
	public void reload() {
		// recharge les elements transient (ici image)
		ImageLoader.loadImage(this.fileName, this.fileName);
		this.image = ImageLoader.get(this.fileName);
	}

	@Override
	public void flip() {
		this.recto = !this.recto;
	}
}
