package elements;

import java.io.Serializable;

import mvc.Jeu;

/**
 * un element de jeu caracterise par une position.
 * 
 * @author vthomas
 *
 */
public abstract class Element implements Dessinable, Serializable {

	/**
	 * serial ID
	 */
	private static final long serialVersionUID = -8027767388545867994L;

	/**
	 * localisation de l'element de jeu
	 */
	int posx, posy;

	/**
	 * creation d'un element de jeu
	 * 
	 * @param px position de l element selon x
	 * @param py position de l'element selon y
	 */
	public Element(int px, int py) {
		this.posx = px;
		this.posy = py;
	}

	// ########################################
	// DEPLACEMENTS ELEMENTS
	// ########################################
	/**
	 * modifier localisation de l'element (coin haut gauche)
	 * 
	 * @param nx nouvelle position selon x
	 * @param ny nouvelle position selon y
	 */
	public void deplacer(int nx, int ny) {
		this.posx = nx;
		this.posy = ny;
	}

	/**
	 * verifie si le point (x,y) se trouve bien dans l'element
	 * 
	 * @param x position du point teste
	 * @param y position du points teste
	 * @return true si le point est dans l'element
	 */
	public abstract boolean isInside(int x, int y);

	// ########################################
	// OPERATIONS DE BASE SUR ELEMENTS
	// ########################################
	/**
	 * activer l element courant (par defaut ne fait rien)
	 * 
	 * @param jeu je courant dans lequel se trouve l'element
	 */
	public abstract void activer(Jeu jeu);

	/**
	 * fait une rotation de l'element courant (par defaut ne fait rien)
	 * 
	 * @param sens sens de rotation
	 */
	public void rotation(boolean sens) {

	}

	/**
	 * melange un element (rien par defaut)
	 */
	public void melanger() {

	}

	/**
	 * permet d'augmenter la taille
	 */
	public void augmenterTaille() {

	}

	/**
	 * permet de diminuer la taille
	 */
	public void diminuerTaille() {

	}

	/**
	 * permet de reinitialiser la taille
	 */
	public void reinitialiserTaille() {

	}

	/**
	 * permet de retourner des elements
	 */
	public abstract void flip();

	// ########################################
	// GESTION SERIALISATION
	// ########################################

	/**
	 * permet de recharge les elements (apres serialisation)
	 */
	public abstract void reload();

}
