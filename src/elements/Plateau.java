package elements;

import java.awt.Graphics;
import java.awt.Image;

import mvc.Jeu;
import tool.ImageLoader;

/**
 * represente un plateau de jeu
 * <li>- localise a un endroit
 * <li>- avec des dimensions
 * <li>- une image associee
 * 
 * @author vthomas
 *
 */
public class Plateau extends Element {

	/**
	 * serial ID
	 */
	private static final long serialVersionUID = 1L;

	/**
	 * l'image du plateau (non serialisee)
	 */
	transient Image bf;
	String fileName;

	/**
	 * dimensions du plateau
	 */
	int dx, dy;

	/**
	 * creation du plateau
	 * 
	 * @param fileName nom du plateau
	 * @param dX       dimension en X
	 * @param dY       dimension en Y
	 * 
	 */
	public Plateau(String fileName, int dX, int dY) {
		// creation element en 0,0
		super(0, 0);

		// sauve nomImage
		this.fileName = fileName;

		// dimensions du plateau
		this.dx = dX;
		this.dy = dY;

		// chargement de l'image a travers la classe ImageLoader
		ImageLoader.loadImage(fileName, fileName);
		this.bf = ImageLoader.get(fileName);
	}

	/**
	 * creation du plateau
	 * 
	 * @param fileName nom du plateau
	 * @param dX       dimension en X
	 * 
	 */
	public Plateau(String fileName, int dX) {
		// element positionne en 0,0
		super(0, 0);

		// chargement de l'image a travers la classe ImageLoader
		ImageLoader.loadImage(fileName, fileName);
		this.bf = ImageLoader.get(fileName);

		// sauve nomImage
		this.fileName = fileName;

		// mise a jour des dimensions
		this.dx = dX;

		// calcul de dy par regle de 3
		int tailleY = this.bf.getHeight(null);
		int tailleX = this.bf.getWidth(null);
		this.dy = (int) ((1.0 * dX * tailleY) / tailleX);
	}

	@Override
	/**
	 * dessine le plateau (interface Dessinable)
	 */
	public void drawElement(Graphics g) {
		g.drawImage(this.bf, posx, posy, dx, dy, null);
	}

	/**
	 * dessine le plateau selectionne (interface Dessinable)
	 */
	public void drawElementSelect(Graphics g) {
		g.drawImage(this.bf, posx, posy, dx, dy, null);
		g.drawRect(posx, posy, dx, dy);
	}

	@Override
	/**
	 * verifie si dans carre
	 */
	public boolean isInside(int x, int y) {
		return (x > this.posx) && (y > this.posy) && (x < this.posx + this.dx) && (y < this.posy + this.dy);
	}

	public void deplacer(int nx, int ny) {
		this.posx = nx - this.dx / 2;
		this.posy = ny - this.dy / 2;
	}

	@Override
	public void reload() {
		// chargement de l'image a travers la classe ImageLoader
		ImageLoader.loadImage(this.fileName, this.fileName);
		this.bf = ImageLoader.get(this.fileName);
	}

	@Override
	public void activer(Jeu jeu) {
		// un plateau ne s'active pas
	}

	@Override
	public void flip() {
		// un plateau ne se retourne pas
	}

}
