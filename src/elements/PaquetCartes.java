package elements;

import java.awt.Color;
import java.awt.Graphics;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import mvc.Jeu;

/**
 * un paquet de cartes
 * <li>- liste de cartes issues d'un repertoire
 * <li>- possibilite de sortir une carte en activant le paquet
 * 
 * @author vthomas
 *
 */
public class PaquetCartes extends Element {

	/**
	 * serial ID
	 */
	private static final long serialVersionUID = 4992511710867902772L;

	/**
	 * Taille du paquet de cartes
	 */
	public static int TAILLE_X = 100;
	public static int TAILLE_Y = 100;

	/**
	 * decalage de carte a la sortie du paquet
	 */
	private static int DECAL_X = TAILLE_X;
	private static int DECAL_Y = 0;
	// increment de decalage a chaque pas de temps
	private static final int DECAL_INCREMENT = 10;

	/**
	 * liste des cartes du paquets
	 */
	List<Carte> cartes;

	/**
	 * ordre du paquet (recto verso)
	 */
	boolean verso = true;

	/**
	 * la taille du paquet de cartes
	 */
	private int taillex;
	private int tailley;

	/**
	 * creation d'un paquet de cartes a partir d'un nom de repertoire
	 * 
	 * @param dirName nom du repertoire
	 */
	public PaquetCartes(String dirName) {
		super(0, 0);

		// taille de ce paquet de cartes
		this.taillex = TAILLE_X;
		this.tailley = TAILLE_Y;

		// recuperation des cartes du repertoire et stockage dans le paquet
		this.cartes = Carte.chargerDesCartes(dirName);
	}

	/**
	 * creation d'un paquet de cartes vide
	 */
	public PaquetCartes() {
		super(0, 0);

		// taille de ce paquet de cartes
		this.taillex = TAILLE_X;
		this.tailley = TAILLE_Y;

		// recuperation des cartes du repertoire et stockage dans le paquet
		this.cartes = new ArrayList<Carte>();
	}

	@Override
	public void drawElement(Graphics g) {

		// forme du paquet
		g.setColor(Color.black);
		g.fillRect(posx - 5, posy - 5, taillex + 10, tailley + 10);

		// si le paquet verso est face cachee
		if (verso) {
			// dessin d'un paquet de carte (vert avec bord noir)
			g.setColor(Color.green);
			g.fillRect(this.posx, this.posy, this.taillex, this.tailley);
			g.setColor(Color.blue);
			g.drawRect(this.posx, this.posy, this.taillex, this.tailley);

			// affiche le nombre de cartes
			g.setColor(Color.black);
			g.drawString("nb: " + this.cartes.size(), this.posx + this.taillex / 2, this.posy + this.tailley / 2);
			// affiche le type
			g.drawString("  Cartes ", this.posx, this.posy + 15);
		} else {
			// paquet face visible

			// s'il reste des cartes
			if (!this.cartes.isEmpty()) {
				// affiche la derniere carte
				Carte c = this.cartes.get(this.cartes.size() - 1);
				c.posx = this.posx;
				c.posy = this.posy;
				c.drawElement(g);
			} else {
				g.setColor(Color.pink);
				g.fillRect(this.posx, this.posy, this.taillex, this.tailley);
			}

			// affiche le nombre de cartes
			g.setColor(Color.black);
			g.drawString("Cartes - nb: " + this.cartes.size(), this.posx, this.posy - 10);
		}

	}

	@Override
	public void drawElementSelect(Graphics g) {
		// dessin du paquet
		this.drawElement(g);
		// encadrement du paquet
		g.setColor(Color.black);
		g.drawRect(posx, posy, this.taillex, this.tailley);
	}

	@Override
	public boolean isInside(int x, int y) {
		return (x > this.posx) && (y > this.posy) && (x < this.posx + this.taillex) && (y < this.posy + this.tailley);
	}

	@Override
	// deplacer au centre du paquet
	public void deplacer(int nx, int ny) {
		this.posx = nx - this.taillex / 2;
		this.posy = ny - this.tailley / 2;

		// reinitialise la sortie des cartes
		DECAL_X = this.taillex;
	}

	@Override
	// active le paquet de cartes => creer une nouvelle carte
	public void activer(Jeu jeu) {

		if (!this.cartes.isEmpty()) {
			Carte c = null;

			// en fonction de l'orientation du paquet (verso / recto)
			if (verso) {
				// jeu face cache => recupere la carte en haut du paquet
				c = this.cartes.get(0);
				this.cartes.remove(0);
			} else {
				// jeu face visible => recupere la carte visible (en bas)
				c = this.cartes.get(this.cartes.size() - 1);
				this.cartes.remove(this.cartes.size() - 1);
			}

			// la positionne a cote du paquet
			c.posx = this.posx + DECAL_X;
			c.posy = this.posy + DECAL_Y;

			// decale le decalage pour ne pas stacker les cartes
			DECAL_X += DECAL_INCREMENT;

			// l'ajoute au jeu
			jeu.addElementVariable(c);
		}
	}

	/**
	 * melange le paquet de cartes
	 */
	public void melanger() {
		Collections.shuffle(this.cartes);
	}

	@Override
	public void reload() {
		// recupere les images des cartes
		for (Carte c : this.cartes)
			c.reload();
	}

	/**
	 * permet d'ajouter une carte a un paquet
	 * 
	 * @param carte carte a ajouter
	 */
	public void ajouterCarte(Carte carte) {
		this.cartes.add(carte);
	}

	@Override
	public void flip() {
		this.verso = !this.verso;

		// changer la taille
		// si verso
		if (verso) {
			this.taillex = TAILLE_X;
			this.tailley = TAILLE_Y;
		} else {
			// si recto = taille de la carte
			if (!this.cartes.isEmpty()) {
				Carte derniereCarte = this.cartes.get(this.cartes.size() - 1);
				this.taillex = derniereCarte.tailleX;
				this.tailley = derniereCarte.tailleY;
			}
		}

	}

}
