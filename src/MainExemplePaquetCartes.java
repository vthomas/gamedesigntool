import elements.Carte;
import elements.Jeton;
import elements.PaquetCartes;
import elements.Plateau;
import mvc.Jeu;

/**
 * classe de test 1. creer un plateau
 * 
 * @author vthomas
 *
 */
public class MainExemplePaquetCartes {

	public static void main(String[] args) {
		// le jeu
		Jeu jeu = new Jeu();

		// ###########################
		// PLATEAU (fixe)
		// ###########################

		// pas de plateau

		// ###########################
		// GESTION PAQUET DE CARTES
		// ###########################

		// choisi la taille des cartes
		Carte.TAILLEX = 100;

		// creer le paquet
		PaquetCartes paquet = new PaquetCartes("images/cartes/");
		// ajoute le paquet
		jeu.addElementVariable(paquet);

		// paquet de cartes vides
		PaquetCartes paquetvide = new PaquetCartes();
		jeu.addElementVariable(paquetvide);

		// ###########################
		// Lancement du jeu
		// ###########################

		// creation de la fenetre de la taille souhaitee
		jeu.creerAppli(1000, 800);

	}

}
