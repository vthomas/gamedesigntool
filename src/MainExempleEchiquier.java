import elements.Jeton;
import elements.Plateau;
import mvc.Jeu;

/**
 * classe de test 1. creer un plateau
 * 
 * @author vthomas
 *
 */
public class MainExempleEchiquier {

	public static void main(String[] args) {
		// le jeu
		Jeu jeu = new Jeu();

		// ###########################
		// PLATEAU (fixe)
		// ###########################

		// creation du plateau (fichier, taille selon x)
		Plateau p = new Plateau("images/echiquier/echiquier.gif", 800);
		// deplace le plateau au milieu
		p.deplacer(500, 400);
		jeu.addElementFixe(p);

		// possibilite d'en faire un plateau deplacable
		// jeu.addElementVariable(p);

		// ###########################
		// JETONS
		// ###########################

		// position initiale des jetons a poser
		Jeton.setPositionInit(50, 300);

		// changer la taille des jetons
		Jeton.TAILLE = 80;

		// ajouter tous les jetons d'un repertoire
		jeu.addTousLesJetons("images/echiquier/pions/");

		// ###########################
		// Lancement du jeu
		// ###########################

		// creation de la fenetre de la taille souhaitee
		jeu.creerAppli(1000, 800);

	}

}
