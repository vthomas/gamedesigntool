import elements.Carte;
import elements.De;
import elements.Jeton;
import elements.PaquetCartes;
import elements.Plateau;
import mvc.Jeu;

/**
 * classe de test 1. creer un plateau
 * 
 * @author vthomas
 *
 */
public class Main {

	public static void main(String[] args) {
		// le jeu
		Jeu jeu = new Jeu();

		// ###########################
		// PLATEAU (fixe)
		// ###########################

		// creation du plateau (fichier, taille selon x)
		Plateau p = new Plateau("images/plateau/arbre.png", 600);
		// deplace le plateau en 100,100
		p.deplacer(500, 400);
		jeu.addElementFixe(p);
		
		// possibilite d'en faire un plateau deplacable
		// jeu.addElementVariable(p);

		// ###########################
		// JETONS
		// ###########################

		// position initiale des jetons a poser
		Jeton.setPositionInit(50, 300);

		// changer la taille des jetons
		Jeton.TAILLE = 100;

		// ajouter les jetons un par un
		jeu.addJeton("images/fruits/cabbage.png");
		jeu.addJeton("images/fruits/prune.png");
		jeu.addJeton("images/fruits/raspberry.png");
		jeu.addJeton("images/fruits/shiny-apple.png");

		// rechanger la taille des jetons
		Jeton.TAILLE = 50;

		// ajouter tous les jetons d'un repertoire
		jeu.addTousLesJetons("images/jetons/");

		// ###########################
		// GESTION DES CARTES
		// ###########################

		// changer la taille des cartes (y est automatique)
		Carte.TAILLEX = 100;

		// ajouter des cartes une a une
		jeu.addCarte("images/cartes/2C.png");
		jeu.addCarte("images/cartes/2D.png");
		jeu.addCarte("images/cartes/2H.png");

		// ajouter toutes les cartes d'un repertoire
		jeu.addToutesLesCarte("images/cartes/");
		
		// ###########################
		// GESTION PAQUET DE CARTES
		// ###########################
		
		PaquetCartes paquet = new PaquetCartes("images/cartes/");
		//ajoute le paquet
		jeu.addElementVariable(paquet);
		
		// ajouter des cartes carres a partir des images de jeton
		PaquetCartes paquet2 = new PaquetCartes("images/jetons/");
		jeu.addElementVariable(paquet2);
		
		//ajouter un paquet vide
		PaquetCartes paquetvide = new PaquetCartes();
		jeu.addElementVariable(paquetvide);

		// ###########################
		// GESTION DES DES
		// ###########################

		// de par defaut (6 faces)
		De de = new De();
		jeu.addElementVariable(de);
		
		// de a 12 faces
		De de12 = new De(12);
		jeu.addElementVariable(de12);
		
		

		// ###########################
		// Lancement du jeu
		// ###########################

		// creation de la fenetre de la taille souhaitee
		jeu.creerAppli(1000, 800);

	}

}
