# 0. updates

La version v1 est valide et utilisable en l'état (préférez faire un main qu'un chargement/sauvegarde). 

Derniers updates (plus récent -> plus vieux)

- ajoute d'un exemple de pavage génère dans un main (MainExemplePavage)
- valide la sauvegarde chargement (version v1)
- possibilité de création d'un paquet de cartes vide
- ajoute la suppression d’éléments
- les paquets peuvent être retournés (face visible)
- les cartes sont rangées dans les paquets
- ajout lock et unlock d'objets (passage d'objets deplaçables à des objets fixes)
- chargement/sauvegarde mis à jour
- modification possible de la taille des objets avec les touches '+' '-'



# 1. Présentation

Ce prototype d'application a pour objectif de créer et manipuler des éléments de jeu au sein d'une fenêtre.
Il constitue un outil qui pourra vous aider à manipuler des éléments de jeu lors du cadre d'une démonstration.

Pour le moment, cet outil permet de gérer
1. des plateaux de jeu;
2. des jetons;
3. des cartes;
4. des paquets de cartes;
5. des dés.

De manière générale, ces éléments sont construit au sein d'un main qui permet de lancer l'application.
Chacun de ces éléments est généré à partir d'image (jeton, carte) que l'application va charger à la création des objets (cf section 5.)
La création des objets se veut (a priori) simple et intuitive.

Une fois l'application lancée, les objets sont manipulable de différentes manières
1. ils sont déplaçables dans l'environnement avec la souris;
2. ils sont activables (en fonction du type d'objet).

Enfin, il est possible de sauvegarder l'état complet du jeu pour le recharger par la suite (afin de préparer facilement une table de jeu).
Les fonctionnalités complètes sont décrites en section 3. (la liste étant amenée à évoluer)

Pour lever toute ambiguïté, il ne s'agit pas de programmer un jeu, mais juste de disposer d'éléments virtuels qui pourraient être faciles à manipuler (a priori plus que via une application de dessin ou un tableur excel comme proposé par certains groupes lundi - mais cela dépend de vos habitudes).

# 2. Installation et lancement

L'application est développée en java 8 et doit fonctionner sur les différents environnements java.

La compilation et l’exécution se font de la manière suivante (à partir de la racine du projet)
```
mkdir bin
javac -cp src -d bin src/Main.java
java -cp bin Main
```
Les sources se trouvent dans le répertoire `src` et sont compilées dans le répertoire `bin`.

Actuellement, les images utilisées se trouvent dans le répertoire images (cf contenu de main.java).

Un Makefile est aussi fourni pour les utilisateurs sous linux avec une opération de compilation et une opération pour le lancement.
```
make compile
make run
```

# 3. Description des Fonctionnalités 

En fonction des objets manipulés, les fonctionnalités sont légèrement différentes.

## Fonctionnalités générales

Les commandes utilisées sont:
- [X] passage de la souris sur un élément pour le sélectionner;
- [X] déplacement des éléments avec un click and drop;
- [X] activation d'un élément avec la touche espace (lorsque l'élément est sélectionne);
- [X] passage d'un élément au premier plan en cliquant dessus
- [X] touche 'b' (pour bottom) => passage d'un élément au dernier plan (tout en bas du plateau) 
- [X] touche 'l' (pour lock) => possibilité de rendre un élément déplaçable -> fixe (non déplaçable) 
- [X] touche 'u' (pour unlock) => possibilité de rendre un élément fixe -> déplaçable (par exemple le plateau)
- [X] touche "suppr" => supprime l’élément sélectionné

## Les objets et leur comportement

Certaines commandes spéciales sont associées à certains objets.
Elles sont listées ci-dessous.

Pour les activer, il faut appuyer sur la touche quand l'objet est sélectionné (souris dessus).

- **Plateau**
	- [X] pas d'activation
- **Carte**
	- [X] création de la carte avec un recto correspondant à une image et un verso rouge
	- [X] ajout d'un ensemble de cartes à la création (à partir d'un répertoire d'images)
	- [X] touches 
		- [X] `espace` => activation = changement de coté de carte (recto / verso)	
		- [X] `r` `a` ou `e` => rotation (`a` et `e` pour les deux sens). Plutôt à faire sur des cartes de forme carré.
		- [X] `+` `-` ou `0` => gestion de la taille des éléments ('0' pour réinitialiser)
		- [X] `f` (flip) => retourne la carte (comme 'espace')
- **Paquet de cartes**
	- [X] création du paquet de cartes à partir des images d'un répertoire
	- [X] si une carte est lâchée sur un paquet => elle est rangée dans le paquet
	- [X] touches
		- [X] `espace` => activation = tirer une carte du paquet
		- [X] `s` (shuffle) => mélange des cartes du paquet
		- [X] `f` (flip) => retourner le paquet (face visible), change aussi l'ordre des cartes avec activation
- **Jeton**
	- [X] création de jeton à partir d'une image
	- [X] création de plusieurs jetons à partir des images d'un répertoire
	- [X] touches
		- [X] `espace` => activation = jeton retourné 
		- [X] `r` `a` `e` => rotation ('a' et 'e' pour les deux sens). Plutôt à faire sur des cartes de forme carré.
		- [X] `+` `-` `0` => gestion de la taille des éléments ('0' pour réinitialiser)
		- [X] `f` (flip) => retourne le jeton (comme 'espace')
- **Dé**
	- [X] dé avec 6 faces par défaut
	- [X] construction de dés avec un nombre de faces quelconques.
	- [X] touches	
		- [X] `espace` = lancement du dé (avec un nouveau résultat)
		- [X] `f` (flip) => retourne le dé : valeur de face opposée
	

Les fonctionnalités listées sont implémentées.

## La création du jeu (ok)

Le fichier ```Main``` fourni donne un exemple de création élément par élément.
Un second fichier ```MainExemple2.java``` fourni permet de générer un jeu d’échec.

## Sauvegarde / chargement (ok)

En plus du main, l'application propose
- un bouton pour sauvegarder l'état complet du jeu.
- un bouton pour charger l'état complet du jeu à partir d'une sauvegarde.

Chargement et sauvegarde sont valides (v1 de l'application).

**remarque** pour pouvoir charger un jeu, il faut que les images soient stockées dans le mème répertoire sinon le jeu ne pourra pas reconstruire les objets.

# 4. Fonctionnalités prévues sur le long terme

Le fichier TODO.txt présentent quelques fonctionnalités supplémentaires.

# 5. Sources

Les images utilisées proviennent des sources suivantes
- https://game-icons.net/ (jetons)
- http://acbl.mybigcommerce.com/52-playing-cards/ (images de cartes)


